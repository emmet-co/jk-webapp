var keystone = require('keystone'),
	async = require('async'),
	_ = require('underscore'),
	moment = require('moment'),
	crypto = require('crypto'),
	Service = keystone.list('Service');

exports.getAll = function(req, res) {
  Service.model.find({'category' : req.params.category_id}).sort('name').exec(function(err, services) {
    if (err) {
      return res.json({
        success: false,
        message: "Nenhum serviço encontrado"
      });
    } else {
        return res.json({
        success: true,
        services : services
      });
    }
  });
};

exports.getOne = function(req, res) {
  Service.model.findById(req.params.service_id).exec(function(err, service) {
    if (err) {
      return res.json({
        success: false,
        message: "Serviço não encontrada"
      });
    } else {
        return res.json({
        success: true,
        service : service
      });
    }
  });
};
