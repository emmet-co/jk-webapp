var keystone = require('keystone'),
	async = require('async'),
	_ = require('underscore'),
	moment = require('moment'),
	crypto = require('crypto'),
  Professional = keystone.list('Professional');

exports.getAll = function(req, res) {
  Professional.model.find().where('services').in([req.params.service_id]).sort('name').exec(function(err, professionals) {
    if (err) {
      return res.json({
        success: false,
        message: "Nenhum profissional encontrado"
      });
    } else {
        return res.json({
        success: true,
        professionals : professionals
      });
    }
  });
};

exports.getOne = function(req, res) {
  Professional.model.findById(req.params.professional_id).exec(function(err, professional) {
    if (err) {
      return res.json({
        success: false,
        message: "Profissional não encontrada"
      });
    } else {
        return res.json({
        success: true,
        professional : professional
      });
    }
  });
};
