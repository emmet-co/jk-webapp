var keystone = require('keystone'),
	async = require('async'),
	_ = require('underscore'),
	moment = require('moment'),
	crypto = require('crypto'),
  Category = keystone.list('Category');

exports.getAll = function(req, res) {
  Category.model.find().sort('name').exec(function(err, categories) {
    if (err) {
      return res.json({
        success: false,
        message: "Categoria não encontrada"
      });
    } else {
        return res.json({
        success: true,
        categories : categories
      });
    }
  });
};

exports.getOne = function(req, res) {
  Category.model.findById(req.params.category_id).exec(function(err, category) {
    if (err) {
      return res.json({
        success: false,
        message: "Categoria não encontrada"
      });
    } else {
        return res.json({
        success: true,
        category : category
      });
    }
  });
};
