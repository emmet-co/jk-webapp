var keystone = require('keystone');
var middleware = require('./middleware');
var express = require('express');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	api: importRoutes('../controllers/')
};

// Setup Route Bindings
exports = module.exports = function(app) {

	// Allow cross-domain requests (development only)
	if (process.env.NODE_ENV != 'production') {
		console.log('------------------------------------------------');
		console.log('Notice: Enabling CORS for development.');
		console.log('------------------------------------------------');
		app.all('*', function(req, res, next) {
			res.header('Access-Control-Allow-Origin', '*');
			res.header('Access-Control-Allow-Methods', 'GET, POST');
			res.header('Access-Control-Allow-Headers', 'Content-Type');
			next();
		});
	}


	// Create our Express router
	var router = express.Router();

	// API
	router.route('/categories/:category_id/services/:service_id/professionals/:professional_id')
	.get(routes.api.professional.getOne);

	router.route('/categories/:category_id/services/:service_id/professionals')
	.get(routes.api.professional.getAll);

	router.route('/categories/:category_id/services')
	.get(routes.api.service.getAll);

	router.route('/categories/:category_id/services/:service_id')
	.get(routes.api.service.getOne);

	router.route('/categories')
  .get(routes.api.category.getAll);

	router.route('/categories/:category_id')
  .get(routes.api.category.getOne);

	// Register all our routes with /api
	app.use('/api/v1', router);

};
