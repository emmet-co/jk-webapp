var keystone = require('keystone');
var Types = keystone.Field.Types;

var Category = new keystone.List('Category', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Category.add({
	name: { type: String, required: true }
});

Category.schema.virtual('content.full').get(function() {
	return this.content.extended || this.content.brief;
});

Category.defaultColumns = 'name';
Category.register();
