var keystone = require('keystone');
var Types = keystone.Field.Types;

var Service = new keystone.List('Service', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Service.add({
	name: { type: String, required: true, initial: true, label: "Nome" },
	description: {type: String, required: false, initial: true, label: "Descrição" },
	duration: { type: Types.Number, default: 30, required: true, initial: true, label: "Duração em minutos:" },
	category: { type: Types.Relationship, ref: 'Category', index: true, required: false, initial: true, label: "Categoria:" }
});

Service.relationship({ ref: 'Category', refPath: 'services', path: 'category' });

Service.defaultColumns = 'name';
Service.register();
