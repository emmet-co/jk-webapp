var keystone = require('keystone');
var Types = keystone.Field.Types;

var Professional = new keystone.List('Professional', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Professional.add({
	name: { type: String, required: true, initial: true, label: 'Nome' },
	cpf: {type: String, initial: true, label: 'CPF'},
	phone: {
		number: { type: String, default: '(11)9', required: true, initial: true, validate: /^(\([0-9]{2}\))\s([9]{1})?([0-9]{4})-([0-9]{4})$/ },
		inMobileApp: {type: Boolean, required: false, default: true, label: 'Celular'}
	},
	services: { type: Types.Relationship, ref: 'Service', initial: true, required: false, many: true, index: true },
});

Professional.relationship({ ref: 'Service', refPath: 'professionals', path: 'services' });

Professional.register();
