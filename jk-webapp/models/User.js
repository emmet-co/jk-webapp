var keystone = require('keystone');
var Types = keystone.Field.Types;

var User = new keystone.List('User');

User.add({
	name: { type: Types.Name, required: true, index: true },
	email: { type: Types.Email, initial: true, required: true, index: true },
	password: { type: Types.Password, initial: true, required: true },
	phone: {
		number: { type: String, default: '(11)9', required: true, initial: true, validate: /^(\([0-9]{2}\))\s([9]{1})?([0-9]{4})-([0-9]{4})$/ },
		inMobileApp: {type: Boolean, required: false, default: true, label: 'Celular:'}
	},
	birthday: { type: Date, required: false, initial: false, label: 'Data de nascimento:'} ,
	sexy: { type: Types.Select, required: false, initial: true, options: 'Masculino, Feminino', label: "Sexo:" },
	cpf: { type: String, required: false, initial: false, label: "CPF:" },
	rg: { type: String, required: false, initial: false, label: "RG:" },
	lastVisit: { type: Date, required: false, initial: false, default: Date.now, label: "Última visita:" },
	dataSheet: { type: Types.Html, wysiwyg: true, required: false, initial: false, label: 'Ficha Técnica:' },
	isVip: { type: Boolean, required: false, initial: false, default: false, label: "Vip:"},
	address: {
		address: {type: String, required: false , initial: true, label: 'Endereço:' },
		addressNumber: { type: Types.Number, required: false, initial: true, label: 'Número:' },
		neighborhood: {type: String, required: false, initial: true, label: 'Bairro:' },
		zipcode: { type: String, required: false, initial: true, label: 'CEP:' },
		city: { type: String, required: false, initial: true, label: 'Cidade:' },
		state: { type: Types.Select, required: false, initial: true, label: 'Estado:', options: 'AC, AL, AP, AM, BA, CE, DF, ES, GO, MA, MT, MS, MG, PA, PB, PR, PE, PI, RJ, RN, RS, RO, RR, SC, SP, SE, TO' }
	}
}, 'Permissions', {
	isAdmin: { type: Boolean, label: 'Can access Keystone', index: true }
});

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function() {
	return this.isAdmin;
});


/**
 * Relationships
 */

User.relationship({ ref: 'Post', path: 'posts', refPath: 'author' });


/**
 * Registration
 */

User.defaultColumns = 'name, email, isAdmin';
User.register();
