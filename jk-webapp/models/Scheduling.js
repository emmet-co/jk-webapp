var keystone = require('keystone');
var Types = keystone.Field.Types;

var Scheduling = new keystone.List('Scheduling', {
	autokey: { from: 'scheduleDate professional customer', path: 'key', unique: true }
});

Scheduling.add({
	initialHour: { type: Types.Datetime, default: Date.now, required: true, initial: true, label: "Hora inicial:" },
	endHour: { type: Types.Datetime, default: Date.now, required: true, initial: true, label: "Hora final:" },
	service: { type: Types.Relationship, ref: 'Service', required: true, initial: true, index: true, label: "Serviço:" },
	professional: { type: Types.Relationship, ref: 'Professional', required: true, initial: true, index: true, label: "Profissional:" },
	customer: { type: Types.Relationship, ref: 'Customer', required: true, initial: true, required: true, index: true, label: "Cliente:" }

});

Scheduling.relationship({ ref: 'Service', refPath: 'schedulings', path: 'service' });
Scheduling.relationship({ ref: 'Professional', refPath: 'schedulings', path: 'professional' });
Scheduling.relationship({ ref: 'Customer', refPath: 'schedulings', path: 'customer' });

Scheduling.schema.virtual('scheduleDate').get(function() {
		return this.initialHour;
})

Scheduling.defaultColumns = 'autokey|20%, scheduleDate|20%, service|20%, professional|20%, customer|20%';
Scheduling.register();
