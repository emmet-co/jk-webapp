var keystone = require('keystone');
var Types = keystone.Field.Types;

var Agreement = new keystone.List('Agreement', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Agreement.add({
	name: { type: String, required: true, initial: true, label: "Nome" },
  publishedDate: { type: Date, default: Date.now, required: true, initial: true, label: "Data de cadastro" }
});

Agreement.defaultColumns = 'name';
Agreement.register();
